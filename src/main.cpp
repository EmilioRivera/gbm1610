#include <iostream>
#include <string>
#include <map>

using namespace std;

float calculateMass(map<char, int>& iteration, map<char, float>& mass, string sequence)
{
	float total = 0.0;
	float waterMass = 18.0;
	float totalWater = 0.0;
	float currentMass = 0.0;

	for each (pair<char, int> line in iteration)
	{
		currentMass = mass[line.first];
		total += currentMass * line.second;
	}

	total = total - waterMass * (sequence.size() - 1);
		 
	return total;
}

void buildSort(map<char, int>& iteration, multimap<int, char>& sort)
{
	for each (pair<char, int> line in iteration)
	{
		sort.insert(pair<int, char>(line.second, line.first));
	}
}

int main()
{
	string sequence = "hevvkfmdvyqrsychpietlvdifqeypdeieyifkpscvplmrcggccndeglecvpteesnitmqimrikphqgqhigemsflqhnkcecrpkkd";

	map<char, int> iteration;
	map<char, float> mass;
	multimap<int, char> sort;

	mass.insert(pair<char, float>('g', 75.0));
	mass.insert(pair<char, float>('a', 89.1));
	mass.insert(pair<char, float>('v', 117.1));
	mass.insert(pair<char, float>('l', 131.2));
	mass.insert(pair<char, float>('i', 131.2));
	mass.insert(pair<char, float>('m', 149.2));
	mass.insert(pair<char, float>('p', 115.1));
	mass.insert(pair<char, float>('f', 165.2));
	mass.insert(pair<char, float>('w', 204.2));
	mass.insert(pair<char, float>('s', 105.1));
	mass.insert(pair<char, float>('t', 119.1));
	mass.insert(pair<char, float>('n', 132.1));
	mass.insert(pair<char, float>('q', 146.1));
	mass.insert(pair<char, float>('y', 181.2));
	mass.insert(pair<char, float>('c', 121.1));
	mass.insert(pair<char, float>('k', 146.2));
	mass.insert(pair<char, float>('r', 174.2));
	mass.insert(pair<char, float>('h', 155.1));
	mass.insert(pair<char, float>('d', 133.1));
	mass.insert(pair<char, float>('e', 147.1));

	for (unsigned int i = 0; i < sequence.size(); i++)
	{
		map<char, int>::iterator it = iteration.find(sequence[i]);
		if (it != iteration.end())
			(it->second)++;
		else
			iteration.insert(pair<char, int>(sequence[i], 1));
	}

	buildSort(iteration, sort);

	float masse = calculateMass(iteration, mass, sequence);
	cout << "Masse de la chaine "  << masse << endl;
	
	auto it3 = sort.rbegin();
	while (it3 != sort.rend())
	{
		cout << it3->second << " avec " << it3->first << " occurences;" << endl;
		it3++;
	}

	system("pause");
	return 0;
}